yum install -y redhat-lsb-core wget rpmdevtools rpm-build createrepo yum-utils vim gcc
yum install -y openssl-devel
cd /root/

#cp /vagrant/nginx-1.20.1-1.el8.ngx.src.rpm . 
wget https://nginx.org/packages/centos/8/SRPMS/nginx-1.20.1-1.el8.ngx.src.rpm

#cp /vagrant/openssl-1.1.1l.tar.gz .
wget https://www.openssl.org/source/openssl-1.1.1l.tar.gz

tar -xvf openssl-1.1.1l.tar.gz
rpm -i nginx-1.20.1-1.el8.ngx.src.rpm
/bin/cp -rf /vagrant/SPECS/nginx.spec ./rpmbuild/SPECS/
yum-builddep -y ./rpmbuild/SPECS/nginx.spec
rpmbuild -bb ./rpmbuild/SPECS/nginx.spec
yum localinstall -y rpmbuild/RPMS/x86_64/nginx-1.20.1-1.el8.ngx.x86_64.rpm
systemctl start nginx
systemctl status nginx
cat >>default.conf <<EOF
server {
    listen       80;
    server_name  localhost;
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
        autoindex on;
    }
}
EOF
/bin/cp -rf default.conf /etc/nginx/conf.d/
mkdir /usr/share/nginx/html/repo
cp rpmbuild/RPMS/x86_64/nginx-1.20.1-1.el8.ngx.x86_64.rpm /usr/share/nginx/html/repo/
createrepo /usr/share/nginx/html/repo/
nginx -t
nginx -s reload
cat >> /etc/yum.repos.d/otus.repo << EOF
[otus]
name=otus-linux
baseurl=http://localhost/repo
gpgcheck=0
enabled=1
EOF
yum list | grep otus
